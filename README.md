swagger: '2.0'
info:
    description: 'The Summary API '
    version: "1.0.0_Beta1"
    title: Summary
basePath: /summary/V1
schemes:
 - https
# Added by API Auto Mocking Plugin
paths:
  /summary:
    get:
      tags:
        - Summary
      summary:  'Get all balances of the contracted products related to a customer '
      description: 'Displays a list of all contracted products of a customer'
      produces: 
        - application/json
      operationId: summary
      parameters:
        - name: Authorization
          in: header
          description: 'The authorization token'
          type: string
          required: true
        - name: page
          in: query
          description: 'Page wich be retrieved'
          type: number
          required: false
        - name: size
          in: query
          description: 'Number of elements to be retrieved'
          type: number
          required: false
      responses:
          '200':
              description: 'Return summary list'
              schema:
                  $ref: '#/definitions/SummaryResponse'
          '400':
              description: 'Bad request.'
              schema:
                  $ref: '#/definitions/EmptyDataResponse'
          '401':
              description: 'Unauthorized. Request was understood but provided credentials info is invalid.'
              schema:
                  $ref: '#/definitions/EmptyDataResponse'
          '403':
              description: 'Forbidden. The credentials info is valid but does not have sufficient rights to perform this operation on the requested resource.'
              schema:
                  $ref: '#/definitions/EmptyDataResponse'
          '404':
              description: 'Summary resource not found.'
              schema:
                 $ref: '#/definitions/EmptyDataResponse'
          '500':
              description: 'Internal server error.'
              schema:
                $ref: '#/definitions/EmptyDataResponse'
definitions:
  SummaryResponse:
    type: object
    properties:
      data:
        type: array
        items:
          $ref: '#/definitions/Contract'
      notifications:
        type: array
        items:
          $ref: '#/definitions/NotificationWrapper'
      paging:
        $ref: '#/definitions/Cursor'
  Contract:
    type: object
    description: 'Basic information about clients product'
    properties:
      key: 
        type: string
        description: 'This Key uniquely identifies this contract'
        example: 'AdwAJahUAOojahPWI9a1AdojOJS'
      alias: 
        type: string
        description: 'This is the user assigned alias for this contract'
        example: 'Mi cuenta'
      category:
        type: "string"
        description: 'CREDIT, UGPRESTAMOS, UGGARRA, BGCUENTAS, etc'
        example: "BGCUENTAS"
      category_description:
        type: string
        description: 'Description for the product category of the contract.'
        example: 'Cuentas'
      product:
        description: 'Product information related to this contract'
        $ref: '#/definitions/Product'
      display_number:
        type: "string"
        description: 'Product contract number anonymized'
        example: '11***2345'
      url:
        type: string
        description: 'The URL of the product instance (contract) referred to here'
        example: '/accounts/{key}'
      status:
        type: string
        description: 'The user understable status for the product.'
        enum:
          - AVAILABLE
          - BLOCKED
          - CLOSED
      balance:
        description: 'This is the main balance used for the type of product. This type of balance varies across products but within each product class this field should represent the same information. For example, the primary balance for a loan product is the outstanding loan value, but it is the total balance for a deposit account.'
        type: object
        $ref: '#/definitions/Money'
  Product:
    type: "object"
    properties:
      type:
        type: 'string'
        description: 'CREDIT_CARD, TRADITIONAL_ACCOUNT, LOANS, etc'
        example: 'TRADITIONAL_ACCOUNT'
      description:
        type: string
        description: 'Description details for this product.'
        example: 'CUENTA TRADICIONAL'
  Money:
    type: object
    properties:
      currency_code:
        type: string
        example: 'MXN'
        description: '[ISO 4217](https://www.iso.org/iso-4217-currency-codes.html) currency code.'
      amount:
        type: number
        format: big-decimal
        description: 'Any monetary amount should take the big-decimal format'
  EmptyDataResponse:
    type: object
    properties:
      data:
        type: string
        example: null
      notifications:
        type: "array"
        items:
          $ref: "#/definitions/NotificationWrapper"
  NotificationWrapper:
      type: "object"
      properties:
        code:
          type: "string"
          description: "Code to identify the notification and classify according the first character (E) Error ,(W) Warning,(I) Info."
          example: "E422CDNPAYRCPTG001"
        message:
          type: "string"
          example: "Something is invalid"
          description: "Notification message  for display to the customer."
        timestamp:
          type: "object"
          format: date-time
          description: "Date and time when the notification is done"
        metadata:
          type: "object"
          properties:
            additionalProp1:
              type: "object"
            additionalProp2:
              type: "object"
            additionalProp3:
              type: "object"
              description: "Additional information required to take actions over received notifications"
  Cursor:
    type: "object"
    properties:
      next_cursor_key:
        type: "string"
        description: "The next cursor key for the set of entries retrieved. This value\
          \ is left absent when there are no more values to return"
        example: "10"
    description: "Cursors object for identifying pagination before and after"